# Python Docker Starter Kit

This Project Helps to understand how to build a minimalist Python project in a Docker environment.

## Prerequisite

* Unix environment like Debian 9 or Macox 10.11 (not tested on Windows)
* Docker Desktop 2.5.x (for Mac)
* Docker Compose (https://docs.docker.com/compose/install/)
* hosts command (https://github.com/alphabetum/hosts) for ./do.sh sethosts
* Acces to remote git repositories
* [direnv](https://direnv.net/)

Install this prerequisites on OSX with [homebrew](https://brew.sh/index_fr):

```
$ brew update
$ brew install direnv
$ brew tap alphabetum/taps && brew install alphabetum/taps/hosts
```

## Try it

	$ make init

Access App http://0.0.0.0:5000


## Display Flask Logs

	$ docker-compose logs -f app
