# Service App

## Run Tests

### Run all tests

	$ docker-compose exec app pytest ./tests -v -s

### Run specific module tests

	$ docker-compose exec app pytest ./tests/test_hello.py -v -s

### Run specific module method tests

	$ docker-compose exec app pytest ./tests/test_hello.py::test_hello -v -s


