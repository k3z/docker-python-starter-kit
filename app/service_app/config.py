import os


def env_var(key, default=None):
    val = os.environ.get(key, default)
    if val == 'True':
        val = True
    elif val == 'False':
        val = False
    return val


DEBUG = env_var('DEBUG', True)
SECRET_KEY = (
    '5030c3b6-3ce9-4c05-a1d4-5cfbe0b09a13-'
    'd48281dc-8866-415e-958f-40dd4576a858'
)

SERVICE_DATA = '/service-data'
SERVICE_STATIC_URL = env_var(
    'SERVICE_STATIC_URL', 'http://static.service.docker'
)
SERVICE_APP_URL = env_var('SERVICE_APP_URL', 'http://api.service.docker')
SERVICE_API_URL = env_var('SERVICE_API_URL', 'http://api:5433/graphql')

POSTGRES_HOST = env_var('POSTGRES_HOST', 'db')
POSTGRES_PORT = env_var('POSTGRES_PORT', '5432')
POSTGRES_DB = env_var('POSTGRES_DB', 'postgres')
POSTGRES_SEARCH_PATH = env_var('POSTGRES_SEARCH_PATH', 'api,graphile_worker')
POSTGRES_USER = env_var('POSTGRES_USER', 'postgres')
POSTGRES_PASSWORD = env_var('POSTGRES_PASSWORD', 'postgres')

MAIL_SERVER = env_var('MAIL_SERVER', 'mailhog')
MAIL_PORT = env_var('MAIL_PORT', 1025)
MAIL_USERNAME = env_var('MAIL_USERNAME', None)
MAIL_PASSWORD = env_var('MAIL_PASSWORD', None)
MAIL_USE_TLS = env_var('MAIL_USE_TLS', False)
MAIL_DEBUG = env_var('MAIL_DEBUG', False)
