import os
import json
from psycopg2.extras import Json

from service_app.postgres_client import pg_open
from service_app.postgres_client import pg_close

here = os.path.abspath(os.path.dirname(__file__))


def load_fixtures():
    conn = pg_open()
    import_data('author', load_data('authors.json'), conn)
    import_data('story', load_data('stories.json'), conn)
    import_data('story_author', load_data('stories_authors.json'), conn)
    import_data('tag', load_data('tags.json'), conn)
    import_data('story_tag', load_data('stories_tags.json'), conn)
    pg_close(conn)


def load_data(file):
    with open(os.path.join(here, file)) as data_file:
        data = json.load(data_file)
    return data


def import_data(table_name, data, conn):
    cursor = conn.cursor()
    columns = ', '.join(list(data[0].keys()))
    values = to_insert_values(data)
    sql = 'INSERT INTO {} ({})\nVALUES {}'.format(table_name, columns, values)
    cursor.execute(sql)
    conn.commit()
    cursor.close()


def to_insert_values(data):
    result = ''
    for i, record in enumerate(data):
        val_list = []
        for k, val in record.items():
            if val is None:
                val = 'NULL'
            if isinstance(val, dict):
                val = Json(val)
            if type(val) == str:
                val = '\'{}\''.format(val)
            val_list += [str(val)]
        result += '({}),\n'.format(', '.join(val_list))
    result = result[:-2] + ";"
    return result
