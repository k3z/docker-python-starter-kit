import psycopg2
import psycopg2.extras
import psycopg2.extensions
import datetime
import select

from service_app.config import POSTGRES_HOST
from service_app.config import POSTGRES_PORT
from service_app.config import POSTGRES_DB
from service_app.config import POSTGRES_USER
from service_app.config import POSTGRES_PASSWORD
from service_app.config import POSTGRES_SEARCH_PATH

db = 'dbname={} user={} password={} host={} port={}'.format(
    POSTGRES_HOST, POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_PORT
)


def pg_open():
    conn = psycopg2.connect(
        dbname=POSTGRES_DB,
        user=POSTGRES_USER,
        password=POSTGRES_PASSWORD,
        host=POSTGRES_HOST,
        port=POSTGRES_PORT,
        options=f'-c search_path={POSTGRES_SEARCH_PATH}',
    )
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    return conn


def pg_close(conn):
    conn.close()


def pg_named_cursor(conn):
    return conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)


def pg_cursor(conn):
    return conn.cursor()
