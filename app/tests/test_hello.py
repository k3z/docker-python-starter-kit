import pytest  # noqa
import requests  # noqa
from .fixtures import reset_fixtures


@pytest.fixture(autouse=True)
def run_around_tests(app):
    # before test
    reset_fixtures()

    yield

    # after test
    reset_fixtures()


def test_hello(client, app):
    foo = 'bar'
    assert 'bar' == foo
