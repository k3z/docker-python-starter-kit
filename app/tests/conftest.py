import pytest
from service_app.app import AppFactory
import requests
from string import Template
from service_app.config import SERVICE_API_URL


def gql_query(query, data={}, headers={}, endpoint=SERVICE_API_URL):
    if bool(data) is True:
        tpl = Template(query)
        query = tpl.substitute(**data)
    return requests.post(endpoint, json={'query': query}, headers=headers)


@pytest.fixture
def app():
    flask_app = AppFactory(test_config={'TESTING': False})

    ctx = flask_app.app_context()
    ctx.push()

    yield flask_app

    ctx.pop()


@pytest.fixture
def client(app):
    """A test client for the app."""
    return app.test_client()


@pytest.fixture
def runner(app):
    """A test runner for the app's Click commands."""
    return app.test_cli_runner()


@pytest.fixture
def data(app):
    return {}
