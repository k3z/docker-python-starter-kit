NOW = $(sh date +%Y%m)

default: here


here:
	@sh -c "pwd"
	@echo "---"
	@echo "Access App http://0.0.0.0:5000"
	@echo "Run cli command $$docker-compose exec app flask service hello"
	@echo "---"

init: install

install:
	@docker-compose pull
	@docker-compose build
	@docker-compose up -d
